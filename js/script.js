window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#237afc"
        },
        "button": {
          "background": "#fff",
          "text": "#237afc"
        }
      },
      "showLink": false,
      "theme": "classic",
      "dismissOnScroll": 100,
      "content": {
        "message": "Strona, którą oglądasz używa plików cookies w celach statystycznych \n\noraz funkcjonalnych. Dalsze przeglądanie strony oznacza zgodę na \n\nich użycie. Pamiętaj, że w każdej chwili masz możliwość wyłączenia \n\nich w swojej przeglądarce.",
        "dismiss": "Rozumiem!"
      }
    })});