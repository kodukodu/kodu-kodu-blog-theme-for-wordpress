</main>

<aside>
    <div class="socialMedia">
        <a href="#">
            <i class="fab fa-bitbucket"></i>
        </a>
        <a href="#">
            <i class="fab fa-github"></i>
        </a>
        <a href="#">
            <i class="fab fa-instagram"></i>
        </a>
        <a href="#">
            <i class="fab fa-linkedin-in"></i>
        </a>
    </div>
    <div class="aboutMe">
        <img src="http://kodukodu.com/wp-content/uploads/2018/07/avatar.jpg" alt="">
        <h3>Kodu Kodu</h3>
        <p>Front End / CSS grid fan</p>
    </div>
    <div id="sidebar" class="widgets-area">
        <?php dynamic_sidebar('sidebar-1'); ?>
    </div>               
</aside>        