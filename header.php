<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Kodu Kodu Blog</title>
    <meta name="author" content="Radek Kodu">
    <meta name="description" content="Kodu Kodu - Blog o nauce programowania, front endzie i tematach luźno związanych z branżą IT.">
    <?php wp_head(); ?>
</head>

<body>
    <div class="container">
        <header>
            <div class="kodu_logo_left">
                <img src="http://kodukodu.com/wp-content/uploads/2018/07/kk_logo_left.png" alt="">
            </div>
            <div class="kodu_logo_right">
                <img src="http://kodukodu.com/wp-content/uploads/2018/07/kk_logo_right.png" alt="">
            </div>
        </header>        
            <nav>
                <?php wp_nav_menu(); ?>
            </nav>
<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
        <main>