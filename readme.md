# Kodu Kodu - personal blog theme for Wordpress
## Info
This is my first theme for Wordpress. It's fully responsive, coded from scratch to learn how to use basics of PHP for Wordpress and CSS grid.

## Technologies

Theme is built with:

* HTML5
* SASS / CSS3
* CSS grid - to keep simple responsive grid
* JS - basic functions for wordpress modules
* PHP - basic PHP taked from Wordpress CODEX
* Wordpress
