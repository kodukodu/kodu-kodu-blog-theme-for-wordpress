<?php get_header(); ?>

<div class="pageContainer">
    <h2 class="--center">Kontakt</h2>
    <p>Jeśli chcesz mnie o coś zapytać, masz jakąś propozycję lub po prostu chcesz się czymś podzielić, śmiało napisz do mnie! </p>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

