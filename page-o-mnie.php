<?php get_header(); ?>

<div class="pageContainer">
    <h2 class="--center">O mnie</h2>
    <p class="">Cześć! mam na imię Radek i uczę się programowania. Obecnie interesuje mnie front end development i jako cel stawiam sobie znalezienie pierwszej pracy związanej właśnie z tą częścią technologi webowych. Moje dotychczasowe doświadczenie zawodowe związane jest z administracją biurową, zmiana pracy będzie więc dla mnie prze... przebra... (nie znoszę tego słowa!) będzie więc dla mnie zmianą zawodu :)</p>
    <p>Celem stworzenia tego bloga było poznanie podstaw tworzenia od zera, motywów pod wordpressa z wykorzystaniem css grid. Kiedy już szablon byl niemal gotowy pomyślałem, że zmotywuję się do regularnego pisania postów. Blog ten poświęcony jest nauce programowania, rozterkami z tym związanymi oraz tematom związanym z szeroko rozumianym IT. Od czasu do czasu piszę na tematy w ogóle nie związane z programowaniem, co nie każdego może zaciekawić, dlatego też na górze znajduje się oddzielna zakładka poza kodem.</p>
    <p>Jeśli chcesz mnie o coś zapytać, masz jakąś propozycję lub po prostu chcesz się czymś podzielić, śmiało napisz do mnie! </p>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

