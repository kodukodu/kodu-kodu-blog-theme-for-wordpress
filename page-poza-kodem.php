<?php get_header(); ?>

<!-- <h2 class="cat--center">Poza kodem</h2> -->

<?php 

    $pozaKodem = new WP_Query('type=post&cat=6');

if( $pozaKodem->have_posts() ):
    
    while( $pozaKodem->have_posts() ): $pozaKodem->the_post(); ?>
        
        <?php get_template_part('content',get_post_format()); ?>
    
        <div class="postContainer">
            <div class="card">
                <?php the_post_thumbnail('full'); ?>    
                <div class="date"><?php the_date(); ?> // 
                    <?php the_tags('#', ' #', '' ); ?>
                </div>
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_content('<button type="button" class="btn">CZYTAJ WIĘCEJ</button>'); ?></p>
                    <!-- <button type="button" class="btn more-link"></button> -->
            </div>

    <?php endwhile;
    
endif;
        
wp_reset_postdata();

?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

