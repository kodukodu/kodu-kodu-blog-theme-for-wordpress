<?php

/*  
========== CSS ==========
*/

function style_script_enqueue() {
  wp_enqueue_style( 'customstyle', get_template_directory_uri() . '/css/kodustyle.css', array(), '1.1', 'all');
  wp_enqueue_style( 'cookie', '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css', array(), '1.1', 'all');
}
add_action( 'wp_enqueue_scripts', 'style_script_enqueue' );

/*
========== JS ==========
*/

function custom_scripts() {
	wp_enqueue_script( 'customscript-cf-cookie', '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js');
	wp_enqueue_script( 'customscript', get_stylesheet_directory_uri() . '/js/script.js');
}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
  

function kodu_theme_setup() {
	
	add_theme_support('menus');
	
	register_nav_menu('primary', 'Primary Header Navigation');
	register_nav_menu('secondary', 'Footer Navigation');
	
}
add_action('init', 'kodu_theme_setup');

add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
add_theme_support('post-formats',array('aside','image','video'));

/*
========== SIDEBAR ===========
*/

function kodu_widget_setup() {

	register_sidebar(
		array(
			'name' => 'Sidebar',
			'id' => 'sidebar-1',
			'class' => 'custom',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		)
	);
}

add_action('widgets_init', 'kodu_widget_setup');

